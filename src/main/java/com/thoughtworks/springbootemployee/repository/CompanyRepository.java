package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Component
public class CompanyRepository {
    private final List<Company> companies = new ArrayList<>();

    public Company save(Company company) {
        company.setId(generateId());
        companies.add(company);
        return company;
    }

    public Long generateId() {
        return companies.stream()
                .max(Comparator.comparingLong(Company::getId))
                .map(company -> company.getId() + 1)
                .orElse(1L);
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public Company getCompanyById(Long id) {
        return companies.stream()
                .filter(company -> id.equals(company.getId()))
                .findFirst()
                .orElse(null);
    }
}
