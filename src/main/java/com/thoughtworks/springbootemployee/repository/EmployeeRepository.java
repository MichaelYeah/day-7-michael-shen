package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class EmployeeRepository {
    private final List<Employee> employees = new ArrayList<>();

    public Employee save(Employee employee) {
        employee.setId(generateId());
        employees.add(employee);
        return employee;
    }

    public Long generateId() {
        return employees.stream()
                .max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public Employee getEmployeeById(Long id) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getId(), id))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getAllEmployeesByCompanyId(Long companyId) {
        return employees.stream()
                .filter(employee -> companyId.equals(employee.getCompanyId()))
                .collect(Collectors.toList());
    }

    public void clear() {
        employees.clear();
    }
}
