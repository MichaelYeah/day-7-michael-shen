package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
    @Resource
    private EmployeeRepository employeeRepository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Employee> getAllEmployees() {
        return employeeRepository.getEmployees();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Employee getEmployeeById(@PathVariable(name = "id") Long id) {
        return employeeRepository.getEmployeeById(id);
    }

    @GetMapping(params = {"gender"})
    @ResponseStatus(HttpStatus.OK)
    public List<Employee> getEmployeesByGender(@RequestParam(name = "gender") String gender) {
        return employeeRepository.getEmployees().stream()
                .filter(employee -> Objects.equals(employee.getGender(), gender))
                .collect(Collectors.toList());
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Employee updateEmployee(@PathVariable(name = "id") Long id, @RequestBody Employee employee) {
        AtomicReference<Employee> return_employee = new AtomicReference<>(new Employee());
        employeeRepository.getEmployees().forEach(employeeFound -> {
                    if (id.equals(employeeFound.getId())) {
                        employeeFound.setAge(employee.getAge());
                        employeeFound.setSalary(employee.getSalary());
                        if (employee.getCompanyId() != null) {
                            Long companyId = employee.getCompanyId();
                            employeeFound.setCompanyId(companyId);
                        }
                        return_employee.set(employeeFound);
                    }
                });
        return return_employee.get();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployee(@PathVariable(name = "id") Long id) {
        employeeRepository.getEmployees().removeIf(employee -> id.equals(employee.getId()));
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> getEmployeesByPaging(@RequestParam("page") int page, @RequestParam("size") int size) {
        int from = (page - 1) * size;
        return employeeRepository.getEmployees().stream()
                .skip(from)
                .limit(size)
                .collect(Collectors.toList());
    }

}
