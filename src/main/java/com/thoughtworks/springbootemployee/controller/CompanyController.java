package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    @Resource
    private CompanyRepository companyRepository;
    @Resource
    private EmployeeRepository employeeRepository;

    @PostMapping
    public Company createCompany(@RequestBody Company company) {
        return companyRepository.save(company);
    }

    @PutMapping("/{id}")
    public Company updateCompanyName(@PathVariable("id") Long id, @RequestBody Company company) {
        AtomicReference<Company> return_company = new AtomicReference<>(new Company());
        companyRepository.getCompanies()
                .forEach(companyFound -> {
                    if (id.equals(companyFound.getId())) {
                        companyFound.setName(company.getName());
                        return_company.set(companyFound);
                    }
                });
        return return_company.get();
    }

    @DeleteMapping("/{id}")
    public void deleteCompany(@PathVariable("id") Long id) {
        companyRepository.getCompanies().removeIf(company -> id.equals(company.getId()));
    }

    @GetMapping
    public List<Company> getAllCompanies() {
        return companyRepository.getCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompany(@PathVariable("id") Long id) {
        return companyRepository.getCompanyById(id);
    }

    @GetMapping("/{companyId}/employees")
    public List<Employee> getAllEmployeesByCompanyId(@PathVariable("companyId") Long companyId) {
        return employeeRepository.getAllEmployeesByCompanyId(companyId);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getCompaniesByPaging(@RequestParam("page") int page, @RequestParam("size") int size) {
        int from = (page - 1) * size;
        return companyRepository.getCompanies().stream()
                .skip(from)
                .limit(size)
                .collect(Collectors.toList());
    }
}
