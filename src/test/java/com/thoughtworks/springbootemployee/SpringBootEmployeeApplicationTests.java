package com.thoughtworks.springbootemployee;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
class SpringBootEmployeeApplicationTests {
	@Autowired
	MockMvc client;

	@Autowired
	EmployeeRepository employeeRepository;

	@BeforeEach
	void setUp() {
		employeeRepository.clear();
	}

	@Test
	void should_get_create_employee_when_perform_create_employee_given_employee() throws Exception {
		//given
		String name = "彭于晏";
		int age = 18;
		String male = "Male";
		int salary = 200;
		long companyId = 1L;
		String employeeJson = "{\n" +
				"    \"name\": \"彭于晏\",\n" +
				"    \"age\": 18,\n" +
				"    \"gender\": \"Male\",\n" +
				"    \"salary\": 200,\n" +
				"    \"companyId\": 1\n" +
				"}";
		//when

		//then
		client.perform(MockMvcRequestBuilders.post("/employees")
						.contentType(MediaType.APPLICATION_JSON)
						.content(employeeJson))
				.andExpect(MockMvcResultMatchers.status().isCreated())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
				.andExpect(MockMvcResultMatchers.jsonPath("$.name").value(name))
				.andExpect(MockMvcResultMatchers.jsonPath("$.age").value(age))
				.andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(male))
				.andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(salary))
				.andExpect(MockMvcResultMatchers.jsonPath("$.companyId").value(companyId));
	}

	@Test
	void should_get_all_employees_when_perform_query_all_employees_given_employee() throws Exception {
		//given
		String name = "彭于晏";
		int age = 18;
		String male = "Male";
		int salary = 200;
		long companyId = 1L;
		Employee employee1 = new Employee(name, age, male, salary, companyId);
		Employee employee2 = new Employee(name, age, male, salary, companyId);
		employeeRepository.save(employee1);
		employeeRepository.save(employee2);

		//when

		//then
		client.perform(MockMvcRequestBuilders.get("/employees"))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(name))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(age))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(male))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(salary))
				.andExpect(MockMvcResultMatchers.jsonPath("$[0].companyId").value(companyId));
	}

	@Test
	void should_get_specific_employee_when_perform_query_employee_by_id_given_employee() throws Exception {
		//given
		String name = "彭于晏";
		int age = 18;
		String male = "Male";
		int salary = 200;
		long companyId = 1L;
		Employee employee1 = new Employee(name, age, male, salary, companyId);
		Employee employee2 = new Employee(name, age, male, salary, companyId);
		Employee savedEmployee1 = employeeRepository.save(employee1);
		Employee savedEmployee2 = employeeRepository.save(employee2);

		//when

		//then
		client.perform(MockMvcRequestBuilders.get("/employees/{id}", savedEmployee2.getId()))
				.andExpect(MockMvcResultMatchers.status().isOk())
				.andExpect(MockMvcResultMatchers.jsonPath("$.id").value(savedEmployee2.getId()))
				.andExpect(MockMvcResultMatchers.jsonPath("$.name").value(name))
				.andExpect(MockMvcResultMatchers.jsonPath("$.age").value(age))
				.andExpect(MockMvcResultMatchers.jsonPath("$.gender").value(male))
				.andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(salary))
				.andExpect(MockMvcResultMatchers.jsonPath("$.companyId").value(companyId));
	}
}
