Objective:
1.	We did group presentation again this morning. At this time, we got a little fuzzier subject to show, which is named OO. But we still communicated with each other tightly and faced the difficulty together.
2.	We then learned HTTP, including its constructure, methods, responses and so on.
3.	In the afternoon, we learn Pair Programming, which is interesting. After that, we start to write codes using Spring Boot. 

Reflective:
1.	This time, we found it hard to group the concept we have listed when we drew concept map.
2.	When doing Pair Programming, I feel that coding with a partner helping me will improve my efficiency.

Interpretive:
1.	Why the OO subject was hard to analyze for our team may are that we didn’t have a deep understanding of it. Although we can list some of the concepts about OO, it is still a tough project to stratified them and find the most logically clear relation among them.
2.	Brothers work together, and their profits break gold.

Decisional:
1.	I think that I should not be content with merely being familiar with the knowledge I learned, but to explore their deeper mechanism, after which I will truly get the hang of them.
2.	I decided to cooperate with my partner better in my future coding career.
